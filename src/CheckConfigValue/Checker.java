/**
 * @file Checker.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

package CheckConfigValue;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Injector.Caster;

public class Checker {

	public static boolean constraintSatisfied (String value, Class type, String constraint) {
		try {
			Caster.convert(type, value);
			if (constraint.isEmpty()) {
				return true;
			}
			Pattern pattern = Pattern.compile(constraint);
			Matcher matcher = pattern.matcher(value);
			if (! matcher.matches()) {
				return false;
			} else {
				return true;
			}
			/*
			if (Caster.convert(type, value).equals(Caster.convert(type, forbidenValue))) {
				return true;
			}*/
		} catch (IllegalArgumentException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
	}

}
