/**
 * @file XMLConfigToTxt.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

package ConfigGenerator;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fileUtilities.WritingUtilities;

import org.w3c.dom.Node;
import org.w3c.dom.Element;

import Model.ConfigurationModel;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class XMLConfigToTxt {

	public static void convertXMLToConfigFile(String xmlFileName, String configFileName) throws ParserConfigurationException, SAXException, IOException {
		List<String> params = new LinkedList<>();

		
		
		/*		
		File fXmlFile = new File(xmlFileName);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);

		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("configurationFile");


		for (int temp = 0; temp < nList.getLength(); temp++) {
			String configurationComment = null;
			String configurationName = null;
			String defaultValue = null;
			String nameSpace = null;

			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				NodeList simpleSectNodes = eElement.getElementsByTagName("option");
				for (int i = 0; i < simpleSectNodes.getLength(); i++) {
					Node nSimpleSectNode = simpleSectNodes.item(i);
					if (nSimpleSectNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElementSimpleSect = (Element) nSimpleSectNode;

						configurationComment = eElementSimpleSect.getElementsByTagName("comment").item(0).getTextContent();
						configurationName = eElementSimpleSect.getElementsByTagName("name").item(0).getTextContent();
						defaultValue = eElementSimpleSect.getElementsByTagName("defaultValue").item(0).getTextContent();
						
						if (eElementSimpleSect.getElementsByTagName("namespace") != null
								&& eElementSimpleSect.getElementsByTagName("namespace").getLength() > 0) {
							nameSpace = "";
							for (int j = 0 ; j < eElementSimpleSect.getElementsByTagName("namespace").getLength() ; j++) {
								nameSpace += eElementSimpleSect.getElementsByTagName("namespace").item(j).getTextContent() + ".";
								
							}
						}
					}
					if (configurationName != null) {
						if (configurationComment != null) {
							params.add("; " + configurationComment);
						}
						if (nameSpace != null) {
							configurationName = nameSpace + configurationName;
						}
						params.add(configurationName + " = " + defaultValue);
						configurationName = null;
					}
				}

			}


		} */
		//WritingUtilities.writeLines(configFileName, params);
	}
}