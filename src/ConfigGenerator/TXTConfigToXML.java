/**
 * @file TXTConfigToXML.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

package ConfigGenerator;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fileUtilities.ReadingUtilities;
import fileUtilities.WritingUtilities;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class TXTConfigToXML {

	public static void convertTxtConfigFile(String xmlFileName, String configFileName) throws ParserConfigurationException, SAXException, IOException {
		List<String> configuration = ReadingUtilities.getLines(configFileName);

		StringBuffer XMLConfig = new StringBuffer();
		XMLConfig.append("<configurationFile>");
		String comment = null;
		String[] configName = null;
		String value = null;

		for (String configLine : configuration) {

			if (configLine.startsWith(";")) {
				if (comment == null) {
					comment = "";
				}
				comment += configLine.substring(configLine.indexOf(";") + ";".length()); 
			} else {
				if (! configLine.isEmpty()) {

					value = configLine.split("=")[1].trim().replace(" ", "");
					XMLConfig.append("<option>")
						.append("<comment>" + comment + "</comment>");
					
					configName = configLine.split("=")[0].trim().replace(" ", "").split("\\.");
					
					for (int i = 0 ; i < configName.length - 1; i++) {
						XMLConfig.append("<namespace>" + configName[i] + "</namespace>");
					}
					XMLConfig.append("<name>" + configName[configName.length - 1] + "</name>");

					XMLConfig.append("<defaultValue>" + value + "</defaultValue>")
					.append("</option>");
				}
			}

		}
		XMLConfig.append("</configurationFile>");

		WritingUtilities.writeLine(xmlFileName, XMLConfig);
	}
}