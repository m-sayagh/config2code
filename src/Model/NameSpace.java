/**
 * @file NameSpace.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import Configuration.Annotations.Config;
import Configuration.Annotations.Config.Support;
import Model.Generator.Visitor.Visitor;

public class NameSpace extends ConfigModel {

	private List<Option> config = null;
	private Map<String, ConfigModel> subConfig = new HashMap<String, ConfigModel>();

	public NameSpace(String pName) {
		super(pName);
	}

	public void addParam(String nameSpaces, Config c) {
		if (nameSpaces != null 
				&& ! nameSpaces.isEmpty()) {

			String[] nameSpace = nameSpaces.split("\\.");	

			if (subConfig == null) {
				subConfig = new HashMap<String, ConfigModel>();
			}

			if (! subConfig.containsKey(nameSpace[0])) {
				subConfig.put(nameSpace[0], new NameSpace(nameSpace[0]));
			} 

			if (nameSpaces.contains(".")) {
				if (subConfig.get(nameSpace[0]) instanceof Option) {
					Option tmp = (Option) subConfig.get(nameSpace[0]) ;
					subConfig.put(nameSpace[0], new NameSpace(nameSpace[0]));
					((NameSpace) subConfig.get(nameSpace[0])).addConfig(tmp);
				}
				((NameSpace) subConfig.get(nameSpace[0]))
				.addParam(nameSpaces.substring(nameSpaces.indexOf(".") + 1), c);
			} else {
				if (subConfig.get(nameSpace[0]) instanceof Option) {
					Option tmp = (Option) subConfig.get(nameSpace[0]) ;
					subConfig.put(nameSpace[0], new NameSpace(nameSpace[0]));
					((NameSpace) subConfig.get(nameSpace[0])).addConfig(tmp);
				} 
				((NameSpace) subConfig.get(nameSpace[0])).addParam(null, c);
			}

		} else {
			if (! subConfig.containsKey(c.name())) {
				subConfig.put(c.name(), new Option(c.name(), c));
			} else {
				if (subConfig.get(c.name()) instanceof NameSpace) {
					((NameSpace) subConfig.get(c.name())).addConfig(new Option(c.name(), c));
				}
			}
		}
	}

	public void accept (Visitor v) {
		v.visit(this);
	}

	public  Map<String, ConfigModel> getSubConfig () {
		return this.subConfig;
	}

	@Override
	public String getOption(String configName, Support type) {
		if (configName.contains(".")) {
			String name = configName.split("\\.")[0];
			if (subConfig.containsKey(name)) {
				return subConfig.get(name).getOption(configName.substring(configName.indexOf(".") + 1), type);
			}
		} else {
			if (subConfig.containsKey(configName)) {
				if (subConfig.get(configName) instanceof Option
						&& (((Option) subConfig.get(configName)).getConfig().support() == type) || type == null) {
					return ((Option) subConfig.get(configName)).getConfig().defaultValue();
				} else {
					if (((NameSpace) subConfig.get(configName)).getConfig() != null) {
						for (Option config : ((NameSpace) subConfig.get(configName)).getConfig()) {
							if (config.getConfig().support() == type || type == null) {
								return config.getConfig().defaultValue();
							}
						}

					}
				}
			}
		}
		return null;
	}

	public List<Option> getConfig() {
		return config;
	}

	public void setConfig(List<Option> config) {
		this.config = config;
	}

	public void addConfig (Option config) {
		if (this.config == null) {
			this.config = new LinkedList<Option>();
		}
		this.config.add(config);
	}
}
