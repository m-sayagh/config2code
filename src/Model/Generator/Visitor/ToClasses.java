/**
 * @file ToClasses.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Model.Generator.Visitor;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import Model.ConfigModel;
import Model.ConfigurationModel;
import Model.NameSpace;
import Model.Option;
import fileUtilities.WritingUtilities;

public class ToClasses implements Visitor {

	private static final String CONFIG_FILE_DEFAULT = "Options.java";
	
	private Stack<String> openNameSpaces = new Stack<>();
	
	private Map<String, List<String>> code = new HashMap<String, List<String>>();
	private String currentConfigFile = CONFIG_FILE_DEFAULT;

	public ToClasses() {
		code.put(CONFIG_FILE_DEFAULT, new LinkedList<String>());
		code.get(currentConfigFile).add("class Options {");
	}

	@Override
	public void visit(ConfigurationModel m) {
		Map<String, ConfigModel> c = m.getConfigurations();
		for (String NameSpacekey : c.keySet()) {

			if (c.get(NameSpacekey) instanceof Option) {
				currentConfigFile = CONFIG_FILE_DEFAULT;
				c.get(NameSpacekey).accept(this);
			} else {
				currentConfigFile = NameSpacekey + ".java";
				if (! code.containsKey(currentConfigFile)) {
					code.put(currentConfigFile, new LinkedList<String>());
					code.get(currentConfigFile).add("class " + NameSpacekey + " {");
				}
				c.get(NameSpacekey).accept(this);
				code.get(currentConfigFile).add("}");
			}
			currentConfigFile = "";
		}
		code.get(CONFIG_FILE_DEFAULT).add("}");
	}

	@Override
	public void visit(NameSpace n) {
		// TODO Auto-generated method stub
	//	code.get(currentConfigFile).add("@ConfigType(name=\"" + n.getName() + "\")");
		
	//	code.get(currentConfigFile).add("class " + n.getName() + " {");
		openNameSpaces.push(n.getName());
		Map<String, ConfigModel> subComfigs = n.getSubConfig();
		for (String key : subComfigs.keySet()) {
			subComfigs.get(key).accept(this);
		}
		openNameSpaces.pop();
//		code.get(currentConfigFile).add("}");
	}

	@Override
	public void visit(Option o) {
		String nameSpace = "";
		Stack<String > tmp = new Stack<>();
		while (! openNameSpaces.isEmpty()) {
			nameSpace = openNameSpaces.peek() + (nameSpace.isEmpty() ? "" : ".") + nameSpace;
			tmp.push(openNameSpaces.pop());
		}
		
		
		while (! tmp.isEmpty()) {
			openNameSpaces.push(tmp.pop());
		}
		
		code.get(currentConfigFile).add(
				"\n\t@Config(name=\"" + o.getName() + "\", \n"
				+ "\t\tdefaultValue=\"" + o.getDefaultValue().replace("\"", "\\\"") + "\""
				+  (nameSpace.isEmpty() ? ")" : ", \n" + "\t\t\tnameSpace=\"" + nameSpace.replace("\"", "\\\"") + "\")"));
		code.get(currentConfigFile).add("\tprivate Object " + nameSpace.replace(".", "_").replace("-", "_").toUpperCase() + "_" + o.getName().toUpperCase() + ";\n");
	}

	@Override
	public List<String> getConfigTxt() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Map<String, List<String>> getConfigFiles() {
		// TODO Auto-generated method stub
		return code;
	}
	
	

}
