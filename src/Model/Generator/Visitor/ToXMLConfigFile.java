/**
 * @file ToXMLConfigFile.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Model.Generator.Visitor;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import Configuration.Annotations.Config;
import Model.ConfigModel;
import Model.ConfigurationModel;
import Model.NameSpace;
import Model.Option;

public class ToXMLConfigFile implements Visitor {

	private List<String> configTxt = new LinkedList<>();
	private String tab = "";

	public void visit(ConfigurationModel m) {
		Map<String, ConfigModel> c = m.getConfigurations();
		configTxt.add("<configuration>");
		tab += "\t";
		for (String NameSpacekey : c.keySet()) {
			c.get(NameSpacekey).accept(this);
		}
		tab = tab.substring(1);
		configTxt.add("</configuration>");
	}

	public void visit(NameSpace n) {
		configTxt.add(tab + "<" + n.getName() + ">");
		tab += "\t";
		Map<String, ConfigModel> subConfig = n.getSubConfig();
		for (String key : subConfig.keySet()) {
			subConfig.get(key).accept(this);
		}
		tab = tab.substring(1);
		configTxt.add(tab + "</" + n.getName() + ">");
	}

	public void visit(Option o) {
		if (o.getConfig().support() == Config.Support.FILE) {
			configTxt.add(tab + "<" + o.getConfig().name() + ">");
			tab += "\t";
			configTxt.add(tab + "<!-- ");
			configTxt.add(tab + o.getConfig().comment());
			if (! o.getConfig().constraint().isEmpty()) {
				configTxt.add(tab + "Constraint : " + o.getConfig().constraint());
			}
			configTxt.add(tab + " -->");
			configTxt.add(tab + o.getConfig().defaultValue());
			tab = tab.substring(1);
			configTxt.add(tab + "</" + o.getConfig().name() + ">");
		}
	}

	public List<String> getConfigTxt() {
		return this.configTxt;
	}

}
