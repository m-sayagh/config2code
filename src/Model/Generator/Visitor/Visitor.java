/**
 * @file Visitor.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Model.Generator.Visitor;

import java.util.List;

import Model.ConfigurationModel;
import Model.NameSpace;
import Model.Option;

public interface Visitor {

	public void visit(ConfigurationModel m);
	public void visit(NameSpace n);
	public void visit(Option o);
	public List<String> getConfigTxt();
	
}
