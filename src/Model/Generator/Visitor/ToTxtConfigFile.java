/**
 * @file ToTxtConfigFile.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Model.Generator.Visitor;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import Configuration.Annotations.Config;
import Model.ConfigModel;
import Model.ConfigurationModel;
import Model.NameSpace;
import Model.Option;

public class ToTxtConfigFile implements Visitor {

	private List<String> configTxt = new LinkedList<>();
	private Stack<String> nameSpacenames = new Stack<>();

	public void visit(ConfigurationModel m) {
		Map<String, ConfigModel> c = m.getConfigurations();
		for (String NameSpacekey : c.keySet()) {
			c.get(NameSpacekey).accept(this);
			configTxt.add("");
		}
	}

	public void visit(NameSpace n) {
		nameSpacenames.push(n.getName());
		Map<String, ConfigModel> subConfig = n.getSubConfig();
		for (String key : subConfig.keySet()) {
			subConfig.get(key).accept(this);
		}
		nameSpacenames.pop();
	}

	public void visit(Option o) {
		if (o.getConfig().support() == Config.Support.FILE) {

			String option = "";
			Stack<String> tmp = new Stack<>();
			while (! nameSpacenames.empty()) {
				String name = nameSpacenames.pop();
				tmp.push(name);
				option = name + "." + option; 
			}
			option = option + o.getConfig().name();
			while (! tmp.empty()) {
				nameSpacenames.push(tmp.pop());
			}

			if (! o.getConfig().constraint().isEmpty()) {
				configTxt.add("; Constraint : " + o.getConfig().constraint());
			}
			if (! o.getConfig().comment().isEmpty()) {
				configTxt.add("; " + o.getConfig().comment());
			}
			configTxt.add(option + " = " + o.getConfig().defaultValue());
		}
	}

	public List<String> getConfigTxt() {
		return this.configTxt;
	}

}
