/**
 * @file ReadSystemConfig.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Model.Readers;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Properties;

import Configuration.Annotations.Config;
import fileUtilities.ReadingUtilities;
import Model.ConfigurationModel;

public class ReadSystemConfig implements IConfigReader {

	@Override
	public void readOptions(String configFileName, ConfigurationModel modelOfValues) {
		Properties prop = System.getProperties();
		
		for (Object key : prop.keySet()) {
			String option = key.toString();
			String name = option;

			String nameSpace = null;
			String confName = null;
			if (name.contains(".")) {
				nameSpace = name.substring(0, name.lastIndexOf("."));
				confName = name.substring(name.lastIndexOf(".") + 1);
			} else {
				confName = name;
			}
			String value = prop.getProperty(option);
			//System.out.println(option + "," + value);
			modelOfValues.addParam(nameSpace, createObject(confName, value));
		}
	}

	private Config createObject(final String configName, final String value) {
		Config r = new Config() {

			@Override
			public Class<? extends Annotation> annotationType() {
				// TODO Auto-generated method stub
				return Config.class;
			}

			@Override
			public Class type() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String name() {
				// TODO Auto-generated method stub
				return configName;
			}

			@Override
			public String constraint() {
				return null;
			}

			@Override
			public String defaultValue() {
				return value;
			}

			@Override
			public String comment() {
				return null;
			}

			@Override
			public String nameSpace() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Support support() {
				// TODO Auto-generated method stub
				return Configuration.Annotations.Config.Support.SYS;
			}
		};
		return r;
	}

}
