/**
 * @file ConfigReader.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Model.Readers;

import Configuration.Annotations.Config.Support;
import Model.ConfigurationModel;


public class ConfigReader {

	private IConfigReader reader;
	private ConfigurationModel modelOfValues = new ConfigurationModel();

	public ConfigReader(String configType,  String confFile) {
		//this.reader = r;
		switch (configType.toLowerCase().trim()) {
		case "flat":
			reader = new FromTxtFile();
			break;
		case "xml":
			reader = new FromXMLFile();
			break;
		default: 
			reader = new FromTxtFile();
			break;
		}
		this.reader.readOptions(confFile, modelOfValues);
		(new ReadSystemConfig()).readOptions(null, modelOfValues);
	}

	public String getOption(String configName, Support type) {
		return modelOfValues.getOption (configName, type);
	}

	public void getConfigOptions(String fileName) {
		/*

		Map<String, String> options = new HashMap<>();

		try {

			TXTConfigToXML.convertTxtConfigFile(fileName + ".value.xml", fileName);

			File fXmlFile = new File(fileName +".value.xml");

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("configurationFile");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				String configurationName = "";
				String defaultValue = null;

				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					NodeList simpleSectNodes = eElement.getElementsByTagName("option");
					for (int i = 0; i < simpleSectNodes.getLength(); i++) {
						Node nSimpleSectNode = simpleSectNodes.item(i);
						if (nSimpleSectNode.getNodeType() == Node.ELEMENT_NODE) {
							Element eElementSimpleSect = (Element) nSimpleSectNode;

							for (int j = 0 ; j < eElementSimpleSect.getElementsByTagName("namespace").getLength(); j ++) {
								configurationName += eElementSimpleSect.getElementsByTagName("namespace").item(j).getTextContent() + ".";
							}

							configurationName += eElementSimpleSect.getElementsByTagName("name").item(0).getTextContent();
							defaultValue = eElementSimpleSect.getElementsByTagName("defaultValue").item(0).getTextContent();
						}
						if (! configurationName.isEmpty()) {
							options.put(configurationName, defaultValue);
							configurationName = "";
							defaultValue = "";
						}
					}
				}
			}

		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		return options;
		 */
	}



}

