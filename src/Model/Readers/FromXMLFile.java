/**
 * @file FromXMLFile.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Model.Readers;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import Configuration.Annotations.Config;
import Model.ConfigurationModel;

public class FromXMLFile implements IConfigReader {

	@Override
	public void readOptions(String configFileName, ConfigurationModel modelOfValues) {
		try {
			File fXmlFile = new File(configFileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc;
			doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("configuration");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				for (int i = 0 ; i < nNode.getChildNodes().getLength() ; i++) {
					Node nChild = nNode.getChildNodes().item(i);
					if (nChild.getNodeType() == Node.ELEMENT_NODE) {
						createModel(nChild, null, modelOfValues);
					}
				}
			}
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	public void createModel (Node n, String configName, ConfigurationModel modelOfValues) {

		String name = n.getNodeName();
		if (n.hasChildNodes()) {
			for (int i = 0; i < n.getChildNodes().getLength() ; i ++) {
				Node child = n.getChildNodes().item(i);
				if (child.getNodeType() != Node.COMMENT_NODE) {
					if (child.getNodeType() != Node.TEXT_NODE) {
						String config = (configName != null ? configName + "." : "") + name;
						createModel(child, config, modelOfValues);
					} else {
						saveConfig(configName, name, child, modelOfValues);
					}
				}
			}
		} else {
			saveConfig(configName, name, n, modelOfValues);
			//			String value = n.getNodeValue();
			//			if (! value.trim().isEmpty()) {
			//				if (configName != null) {
			//					modelOfValues.addParam(configName, createObject(configName + "." + name, value.trim()));
			//				} else {
			//					modelOfValues.addParam(configName, createObject(name, value.trim()));
			//				}
			//			}
		}
	}


	private void saveConfig(String configName, String name, Node n, ConfigurationModel modelOfValues) {
		String value = n.getNodeValue();
		if (! value.trim().isEmpty()) {
			modelOfValues.addParam(configName, createObject(name, value.trim()));
		}
	}

	private Config createObject(final String configName, final String value) {
		Config r = new Config() {

			@Override
			public Class<? extends Annotation> annotationType() {
				// TODO Auto-generated method stub
				return Config.class;
			}

			@Override
			public Class type() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String name() {
				// TODO Auto-generated method stub
				return configName;
			}

			@Override
			public String constraint() {
				return null;
			}

			@Override
			public String defaultValue() {
				return value;
			}

			@Override
			public String comment() {
				return null;
			}

			@Override
			public String nameSpace() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Support support() {
				// TODO Auto-generated method stub
				return Configuration.Annotations.Config.Support.FILE;
			}
			
		};
		return r;
	}


}
