/**
 * @file FromTxtFile.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Model.Readers;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Properties;

import Configuration.Annotations.Config;
import fileUtilities.ReadingUtilities;
import Model.ConfigurationModel;

public class FromTxtFile implements IConfigReader {
	private static String loadedFile = null;
	private static Properties props = new Properties();

	@Override
	public void readOptions(String configFileName, ConfigurationModel modelOfValues) {
		FileInputStream istream = null;
		try {
			//			if (loadedFile  == null || ! loadedFile.equals(configFileName)){
			loadedFile = configFileName;
			istream = new FileInputStream(configFileName);
			props.load(istream);

			//		}
			for (Object key : props.keySet()) {

				String name = key.toString().trim();

				String nameSpace = null;
				String confName = null;
				if (name.contains(".")) {
					nameSpace = name.substring(0, name.lastIndexOf("."));
					confName = name.substring(name.lastIndexOf(".") + 1);
				} else {
					confName = name;
				}

				String value = props.getProperty(key.toString());
				modelOfValues.addParam(nameSpace, createObject(confName, value));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (istream != null) {
					istream.close();
				}
			} catch (Exception e) {}
		}
	}

	private Config createObject(final String configName, final String value) {
		Config r = new Config() {

			@Override
			public Class<? extends Annotation> annotationType() {
				// TODO Auto-generated method stub
				return Config.class;
			}

			@Override
			public Class type() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String name() {
				// TODO Auto-generated method stub
				return configName;
			}

			@Override
			public String constraint() {
				return null;
			}

			@Override
			public String defaultValue() {
				return value;
			}

			@Override
			public String comment() {
				return null;
			}

			@Override
			public String nameSpace() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Support support() {
				// TODO Auto-generated method stub
				return Configuration.Annotations.Config.Support.FILE;
			}
		};
		return r;
	}

}
