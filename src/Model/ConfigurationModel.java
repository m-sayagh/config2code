/**
 * @file ConfigurationModel.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Model;

import java.util.HashMap;
import java.util.Map;

import Configuration.Annotations.Config;
import Model.Generator.Visitor.Visitor;

public class ConfigurationModel {

	private Map<String, ConfigModel> configurations = null;

	public Map<String, ConfigModel> getConfigurations() {
		return configurations;
	}

	public void addParam(String nameSpaces, Config c) {
		if (configurations == null) {
			configurations = new HashMap<String, ConfigModel>();
		}

		if (nameSpaces != null && ! nameSpaces.trim().isEmpty()) {
			String[] nameSpace = nameSpaces.split("\\.");

			initiateConfigurations(nameSpace[0]);

			if (nameSpaces.contains(".")) {
				((NameSpace) configurations
						.get(nameSpace[0]))
						.addParam(
								nameSpaces.substring(nameSpaces.indexOf(".") + 1), 
								c);
			} else {
				((NameSpace) configurations
						.get(nameSpace[0]))
						.addParam(
								null, 
								c);
			}

		} else {
			if (! configurations.containsKey(c.name())) {
				configurations.put(c.name(), new Option(c.name(), c));
			} else {
				if (configurations.get(c.name()) instanceof NameSpace) {
					((NameSpace) configurations.get(c.name())).addConfig(new Option(c.name(), c));
				}
			}
		}
	}

	public void initiateConfigurations(String nameSpace) {

		if (! configurations.containsKey(nameSpace)) {
			configurations.put(nameSpace, new NameSpace(nameSpace));
		} else {
			if (configurations.get(nameSpace) instanceof Option) {
				Option tmp = ((Option) configurations.get(nameSpace));
				configurations.put(nameSpace, new NameSpace(nameSpace));
				((NameSpace) configurations.get(nameSpace)).addConfig(tmp);
			}

		}
	}

	public void accept (Visitor v) {
		v.visit(this);
	}

	public String getOption(String configName, Config.Support type) {
		if (configName.contains(".")) {
			String name = configName.split("\\.")[0];
			if (configurations.containsKey(name)) {
				return configurations
						.get(name)
						.getOption(configName.substring(configName.indexOf(".") + 1), type);
			}
		} else {
			if (configurations.containsKey(configName)) {
				if (configurations.get(configName) instanceof Option) {
					if (((Option) configurations.get(configName)).getConfig().support() == type) {
						return ((Option) configurations.get(configName)).getConfig().defaultValue();
					}
				} else {
					if (((NameSpace) configurations.get(configName)).getConfig() != null)
						for (Option config : ((NameSpace) configurations.get(configName)).getConfig()) {
							if (config.getConfig().support() == type) {
								return config.getConfig().defaultValue();
							
						}
					}
				}
			}
		}
		return null;
	}

}
