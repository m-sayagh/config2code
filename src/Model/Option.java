/**
 * @file Option.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Model;

import Configuration.Annotations.Config;
import Configuration.Annotations.Config.Support;
import Model.Generator.Visitor.Visitor;

public class Option extends ConfigModel {

	private Config config ;
	private String value;

	public Option(String pName, Config c) {
		super(pName);
		setConfig(c);
	}

	public Option(String pName, String v) {
		super(pName);
		setValue(v);
	}



	@Override
	public String getOption(String configName, Support type) {
		if (type == null || config.support() == type)
			return this.config.defaultValue(); 
		else 
			return null;
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	public String getValue() {
		return value;
	}

	public String getDefaultValue () {
		return this.config.defaultValue();
	}

	public Object getType () {
		if (this.config.type() != null && this.config.type() != Object.class) {
			return this.config.type();
		} else {

		}
		return this.config.type();
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void accept (Visitor v) {
		v.visit(this);
	}

}
