/**
 * @file Arguments.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

package fileUtilities;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Arguments {

	Map<String, String> parametersFormat = new HashMap();
	Map<String, String> parametersData = new HashMap();
	

	/**
	 * This function takes as input a parameters format
	 * @param parameters: the possible parameters, example -f:file name,-p:package name
	 */
	public Arguments(String parameters) {

		String[] params = parameters.split(",");
		for (String param : params) {
			if (param.contains(":")) {
				parametersFormat.put(param.split(":")[0], param.split(":")[1]);
			} else {
				parametersFormat.put(param, "");
			}
		}
		
	}
	
	public void setParametersData (String[] args) {
		if (args == null || args.length == 0) {
			return ;
		}
		String currentParam = "";
		for (int i = 0 ; i < args.length ; i++ ) {
			if (parametersFormat.keySet().contains(args[i])) {
				currentParam = args[i];
			} else {
				if (! currentParam.isEmpty())  {
					parametersData.put(currentParam, args[i]);
				}
				currentParam = "";
			}
		}
	}
	
	public String getParameter (String p) {
		if (! p.isEmpty() 
				&& parametersData != null 
				&& parametersData.size() > 0) {
			return parametersData.get(p);
		} else {
			return null;
		}
	}

	public String help () {
		String results = "";
		for (String p : parametersFormat.keySet()) {
			results += p + " : " + parametersFormat.get(p) + "\n";
		}
		return results;
	}
	
	public String printArguments () {
		String results = "";
		for (String p : parametersData.keySet()) {
			results += p + " : " + parametersData.get(p) + "\n";
		}
		return results;
	}
	
}
