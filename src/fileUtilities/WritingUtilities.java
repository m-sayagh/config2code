/**
 * @file WritingUtilities.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package fileUtilities;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

public class WritingUtilities {


	public static void writeLines (String pFileName, List<String> pLines ) {

		PrintWriter writer;
		try {
			writer = new PrintWriter(pFileName);

			for (String string : pLines) {
				writer.println(string);
			}

			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void writeLine (String pFileName, StringBuffer pLine ) {

		PrintWriter writer;
		try {
			writer = new PrintWriter(pFileName);

			writer.println(pLine);

			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void serializeMap (String file, Map map) {
		try
		{
			FileOutputStream fos = new FileOutputStream(file + ".ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(map);
			oos.close();
			fos.close();

		}catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	public static void serializeList(String file, List traceLines) {
		try
		{
			FileOutputStream fos = new FileOutputStream(file + ".ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(traceLines);
			oos.close();
			fos.close();

		}catch(IOException ioe)
		{
			ioe.printStackTrace();
		}		
	}



}
