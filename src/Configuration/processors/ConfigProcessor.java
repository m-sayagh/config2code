/**
 * @file ConfigProcessor.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Configuration.processors;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;

import Model.Generator.Visitor.ToTxtConfigFile;
import Model.Generator.Visitor.ToXMLConfigFile;
import Model.Generator.Visitor.Visitor;
import Model.Readers.FromTxtFile;
import Model.Readers.FromXMLFile;
import Model.Readers.IConfigReader;


@SupportedAnnotationTypes(value={"Configuration.Annotations.Config"})
@SupportedSourceVersion(SourceVersion.RELEASE_7)
public class ConfigProcessor extends AbstractProcessor {

	private Messager messager;
	private Filer filer;
	private Map<String, String> options;
	private List<IConfigProcessor> processors = new LinkedList<>();

	public void init(ProcessingEnvironment env) {

		messager = env.getMessager();
		filer = env.getFiler();
		options = env.getOptions();
		generateInitClass();

		if (options.containsKey("config.generate.config.file") 
				&& options.get("config.generate.config.file").equals("true")) {
			if (options.containsKey("config.generate.config.file.name")) {
				Visitor v = new ToTxtConfigFile();

				if (options.containsKey("config.generate.config.file.format") ) {
					switch (options.get("config.generate.config.file.format").toLowerCase()) {
					case "flat":
						v = new ToTxtConfigFile();
						break;
					case "xml":
						v = new ToXMLConfigFile();
						break;
					}
				}
				processors.add(new Generator(messager, filer, options.get("config.generate.config.file.name"), v));
			}
		}

		if (options.containsKey("config.inject.values")
				&& options.get("config.inject.values").equals("true")) {
			if (options.containsKey("config.generate.config.file.name")) {

				String readerType = "flat";
				if (options.containsKey("config.generate.config.file.format") ) {
					readerType = options.get("config.generate.config.file.format").toLowerCase();
				}

				processors.add(new ConfigInjectorProcessor(filer, options.get("config.generate.config.file.name"), readerType));
			}
		}

		if (options.containsKey("config.values.checker")
				&& options.get("config.values.checker").equals("true")) {
			if (options.containsKey("config.generate.config.file.name")) {
				String readerType = "flat";
				if (options.containsKey("config.generate.config.file.format") ) {
					readerType = options.get("config.generate.config.file.format").toLowerCase();
				}
				processors.add(new ConfigValueChecker(filer, options.get("config.generate.config.file.name"), readerType));
			}
		}

		if (options.containsKey("config.check.style") 
				&& options.get("config.check.style").equals("true")) {
			if (options.containsKey("config.check.style.format")) {
				processors.add(new CheckStyle(messager, options.get("config.check.style.format")));
			}
		}
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		for (IConfigProcessor p : processors) {
			p.process(annotations, roundEnv);
		}
		return true;
	}

	public void generateInitClass () {

		String initCode= "package Config2Code;"
				+ "public class Config2Code {\n";
		initCode += "\tpublic static void init () {\n";
		if (options.containsKey("config.inject.values")
				&& options.get("config.inject.values").equals("true")) {
			initCode += "\t\tinjection.inject();\n";
		}
		if (options.containsKey("config.values.checker")
				&& options.get("config.values.checker").equals("true")) {
			initCode += "\t\tconfigValueChecker.checkValues();\n";
		}
		initCode += "\t}\n";
		initCode += "}\n";

		try {
			JavaFileObject file = filer.createSourceFile("Config2Code/Config2Code");
			file.openWriter().append(initCode).close();
		} catch (IOException ioe) {
		}
	}

}
