/**
 * @file ConfigInjectorProcessor.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */



package Configuration.processors;
import java.io.File;
import java.io.IOException;
import java.util.Set;

import javax.annotation.processing.Filer;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;

import Configuration.Annotations.Config;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.LoaderClassPath;
import javassist.NotFoundException;


public class ConfigInjectorProcessor implements IConfigProcessor {

	private Filer filer;
	private String configFile;
	private String configType;

	public ConfigInjectorProcessor(Filer pFiler, String pConfigFile, String pConfigType) {
		filer = pFiler;
		configFile = pConfigFile;
		configType = pConfigType;
	}

	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment env) {
		/*
		String aspectText =
				"public aspect injector {\n"
						+ "  after() : initialization(*.new(..)) && ! within(injector) {\n"
						+ "    Injector.ConfigInjector.injectConfig(thisJoinPoint.getTarget(), \"" + configFile + "\" , \"" + configType + "\" );\n" 
						+ "  }\n"
						+ "}\n";
		try {
			JavaFileObject file = filer.createSourceFile("injector");
			file.openWriter().append(aspectText).close();

		} catch (IOException ioe) {
		//	ioe.printStackTrace();
		}
		 */
		String code = "package Config2Code;\n"
				+ "import javassist.ClassPool;\n"
				+"import javassist.CtClass;\n"
				+"import javassist.CtConstructor;\n"
				+"import javassist.NotFoundException;\n"
				+ "public class injection {\n";
		code += "\tpublic static void inject() {\n";
		for (TypeElement annotation : annotations) {
			Set<? extends Element> elements = env.getElementsAnnotatedWith(annotation);
			for (Element element : elements) {
				Config ann = ((Config) element.getAnnotation(Config.class));
				if (ann != null) {
					Element elem = element;

					while (elem != null && elem.getKind() != ElementKind.CLASS) {
						elem = elem.getEnclosingElement();
					}

					TypeElement classElement = (TypeElement) elem;
					PackageElement packageElement =
							(PackageElement) classElement.getEnclosingElement();
					String packageName = packageElement.getQualifiedName().toString();
					code += "\t\tinstrument "
							+ "( \"" 
							+ (packageName.isEmpty() ? "" : packageName + "." ) + classElement.getSimpleName().toString() 
							+ "\" , \"" 
							+ configFile 
							+ "\" , \"" 
							+ configType + "\");\n";
					//	instrument(classElement.getSimpleName().toString());

				}
			}
		}
		try {
			code += "\n\t}\n";

			code += "\tpublic static void instrument (String className , String configFile, String configType)  {\n"

		+ "\t\ttry {\n"
		+ "\t\t\tClassPool pool = ClassPool.getDefault();\n"
		+ "\t\t\tCtClass cc;\n"
		+ "\t\t\tcc = pool.get(className);\n"
		+ "\t\t\tCtConstructor[] m = cc.getDeclaredConstructors();\n"
		+ "\t\t\tfor (int i = 0 ; i < m.length ; i++) {\n"
		+	"\t\t\t\tm[i].insertAfter(\"Injector.ConfigInjector.injectConfig(this, \\\"\" + configFile + \"\\\", \\\"\" + configType + \"\\\");\");\n"

			+ "\t\t\t}\n"
			+ "\t\t\tcc.toClass();\n"
			+ "\t\t} catch (Exception e) {\n"
			+ "\t\t}\n"
			+"\t}\n"; 

			code += "\n}\n";
			JavaFileObject file = filer.createSourceFile("Config2Code/injection");
			file.openWriter().append(code).close();

		} catch (IOException ioe) {
			//	ioe.printStackTrace();
		}
		return true;
	}

	/*
	public static void instrument (String className , String configFile, String configType)  {

		try {
			ClassPool pool = ClassPool.getDefault();
			CtClass cc;
			cc = pool.get(className);
			CtConstructor[] m = cc.getConstructors();
			for (int i = 0 ; i < m.length ; i++) {
				m[i].insertAfter("Injector.ConfigInjector.injectConfig(this, \"" 
						+ configFile + "\", \"" + configType + "\");");
			}
			cc.toClass();
		} catch (Exception e) {
		}
	}*/
}
