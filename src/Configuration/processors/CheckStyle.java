/**
 * @file CheckStyle.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Configuration.processors;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;

import ConfigCheckStyle.CheckOptionName;
import ConfigCheckStyle.CheckOptionType;
import ConfigCheckStyle.IChecker;
import ConfigCheckStyle.PersonalizedChecker;

import javax.lang.model.element.Element;

public class CheckStyle implements IConfigProcessor {


	private Messager messager;

	private List<IChecker> checkers = new LinkedList<>();

	public CheckStyle (Messager pMessager, String configFormat) {
		messager =  pMessager;
		checkers.add(new PersonalizedChecker(configFormat));
		/*
		checkers.add(new CheckOptionName());
		checkers.add(new CheckOptionType());
		*/
	}

	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment env) {
		for (TypeElement annotation : annotations) {
			Set<? extends Element> elements = env.getElementsAnnotatedWith(annotation);
			for (Element element : elements) {
				for (IChecker check : checkers) {
					check.check(element, messager);
				}
			}
		}
		return true;
	}

}
