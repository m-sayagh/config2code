/**
 * @file Generator.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

package Configuration.processors;

import java.util.Set;

import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;

import Configuration.Annotations.Config;
//import Configuration.Annotations.ConfigType;
import Model.ConfigurationModel;
import Model.Generator.Visitor.ToTxtConfigFile;
import Model.Generator.Visitor.Visitor;
import fileUtilities.WritingUtilities;

import javax.lang.model.element.Element;

public class Generator implements IConfigProcessor {

	private String configFileName;
	private ConfigurationModel m = null;
	private Visitor v;
	public Generator(Messager pMessager, Filer pFiler, String configFileName, Visitor outputFormat) {
		this.configFileName = configFileName;
		this.v = outputFormat;
	}

	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment env) {
		m = new ConfigurationModel();
		for (TypeElement annotation : annotations) {
			Set<? extends Element> elements = env.getElementsAnnotatedWith(annotation);
			for (Element element : elements) {
				Config ann = ((Config) element.getAnnotation(Config.class));
				if (ann != null) {
					Element e = element;
					String nameSpace = ann.nameSpace();
					if (nameSpace.isEmpty()) {
						nameSpace = null;
					}
					m.addParam(nameSpace, ann);
				}
			}
		}
		v.visit(m);
		WritingUtilities.writeLines(configFileName, v.getConfigTxt());
		return true;

	}

}
