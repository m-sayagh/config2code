/**
 * @file ConfigValueChecker.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

package Configuration.processors;

import java.io.IOException;
import java.util.Set;

import javax.annotation.processing.Filer;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;

import Configuration.Annotations.Config;
//import Configuration.Annotations.ConfigType;

public class ConfigValueChecker implements IConfigProcessor {

	private String ConfigFileName = ""; 
	private Filer filer;
	private String reader;

	private static boolean aspectCreated = false;

	public ConfigValueChecker(Filer filer, String pConfigFile, String pReaderType) {
		ConfigFileName = pConfigFile;
		this.filer = filer;
		this.reader = pReaderType;
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment env) {

		String aspectText =
				"package Config2Code;\n"
				+ "public class configValueChecker {\n"
						+ "  public static void checkValues() {\n"
						+ "    Model.Readers.ConfigReader reader = new Model.Readers.ConfigReader(\"" + this.reader + "\",  \"" + ConfigFileName + "\");\n";
		//	+ "    java.util.Map<String, String> options = Injector.ConfigReader.getConfigOptions(\"" + ConfigFileName + "\");\n";

		for (TypeElement annotation : annotations) {
			Set<? extends Element> elements = env.getElementsAnnotatedWith(annotation);
			for (Element element : elements) {
				Config ann = ((Config) element.getAnnotation(Config.class));
				if (ann != null && ann.constraint() != null) {
					String nameSpace = ann.nameSpace();
					if (! nameSpace.isEmpty()) {
						nameSpace += ".";
					}
					/*
					Element e1 = element.getEnclosingElement();
					while(e1 != null) {
						ConfigType nameSpaceAnnotation = e1.getAnnotation(ConfigType.class);
						if (nameSpaceAnnotation != null) {
							nameSpace = nameSpaceAnnotation.name() + "." + nameSpace;
						}
						e1 = e1.getEnclosingElement();
					}*/
					try {
						String type = element.asType().toString();
						aspectText += 
								"    if (! CheckConfigValue.Checker.constraintSatisfied(reader.getOption(\"" 
										+ nameSpace + ann.name() + "\"" + ", Configuration.Annotations.Config.Support." + ann.support() + "), " + type + ".class , \"" + ann.constraint().replace("\\", "\\\\") + "\")) {\n" 
										+ "      System.err.println(\"The constraint '" + ann.constraint().replace("\\", "\\\\") + "' is not satisfied for: " + nameSpace + ann.name() + "\");\n"
										+ "      System.exit(-1);\n"
										+ "    }\n";
					} catch (Exception e) {
						aspectText += e.getMessage();
					}
				}
			}
		}

		aspectText += "  }\n"
				+ "}\n";
		try {
			JavaFileObject file = filer.createSourceFile("Config2Code/configValueChecker");
			file.openWriter().append(aspectText).close();
		} catch (IOException ioe) {
		}

		return true;
	}

}
