/**
 * @file Config.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

package Configuration.Annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
/**
 * Annotation used to define configuration options in the source code. 
 * It is designed to annotate variables and attributes that contain a configuration value.
 * 
 * The value of this option will be read and injected in this variable. 
 * 
 * @author Mohammed
 */
public @interface Config {
	
	public enum Support {
		FILE, ARGS, SYS
	}
	
	/**
	 * Name of the configuration option, this name will appear in the configuraiton file 
	 */
	String name() default "";
	/** 
	 * Comment for the option to help users understanding it in the configuration file
	 */
	String comment() default "";
	/**
	 * Type of the configuration option
	 */
	Class type() default Object.class;
	/**
	 * The configuration option default value
	 */
	String defaultValue () default "";
	/** 
	 * Forbidden value. This value is used to avoid critical errors and crashes
	 */
	String constraint() default "";
	/**
	 * NameSpace of a configuration option
	 */
	String nameSpace() default "";
	/**
	 * Configuration Support (file, main args, system)
	 */
	Support support() default Support.FILE;
}
