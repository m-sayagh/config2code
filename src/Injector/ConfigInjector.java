/**
 * @file ConfigInjector.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Injector;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import Configuration.Annotations.Config;
//import Configuration.Annotations.ConfigType;
import fileUtilities.WritingUtilities;
import Model.Readers.ConfigReader;
import Model.Readers.FromTxtFile;
import Model.Readers.FromXMLFile;
import Model.Readers.IConfigReader;

public class ConfigInjector {

	private static ConfigReader config = null;

	public static void injectConfig (Object o, String configFile, String configReaderType) {
		if (configFile != null ) {
			config = new ConfigReader(configReaderType.toLowerCase().trim(), configFile);
			try {
				Field[] annotations = o.getClass().getDeclaredFields();
				for (Field field : annotations) {
					if (field.isAnnotationPresent(Config.class)) {
						boolean accessible = field.isAccessible();
						field.setAccessible(true);
						String configName = field.getAnnotation(Config.class).name().trim();
						String nameSapce = field.getAnnotation(Config.class).nameSpace();
						configName = nameSapce + (nameSapce.isEmpty() ? "" : ".") + configName;

						field.set(o, 
								Caster.convert(
										field.getType() ,
										config.getOption(configName, field.getAnnotation(Config.class).support())
										)
								);
						field.setAccessible(accessible);
					}
				}

			} catch (SecurityException
					| IllegalArgumentException 
					| IllegalAccessException e) {

			}
		}
	}

}
