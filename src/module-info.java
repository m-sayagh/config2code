/**
 * @file module-info.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


module config2code {
	exports ConfigGenerator;
	exports Configuration.Injector.Flats;
	exports Configuration.Annotations;
	exports Model.Generator.Visitor;
	exports Configuration.Checker;
	exports CheckConfigValue;
	exports fileUtilities;
	exports Configuration.Injector;
	exports Injector;
	exports Configuration.processors;
	exports Model;
	exports Model.Readers;
	exports ConfigCheckStyle;

	requires java.compiler;
	requires java.xml;
	requires javassist;
}