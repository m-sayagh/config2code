/**
 * @file PersonalizedChecker.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */

package ConfigCheckStyle;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.processing.Messager;
import javax.tools.Diagnostic.Kind;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import Configuration.Annotations.Config;

public class PersonalizedChecker implements IChecker {

	List<CheckModule> checkModules = new LinkedList<>();

	public PersonalizedChecker(String formatFile) {
		try {
			checkModules = readCheckFormatsXML(formatFile);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean check(javax.lang.model.element.Element e, javax.annotation.processing.Messager m) {

		for (CheckModule checkModule : checkModules) {
			checkModule.check(e, m);
		}

		return true;
	}

	private List<CheckModule> readCheckFormatsXML(String xmlFile) throws ParserConfigurationException, SAXException, IOException {
		List<CheckModule> results = new LinkedList<>();

		File fXmlFile = new File(xmlFile);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);

		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("module");

		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				results.add(new CheckModule(eElement));
			}
		}

		return results;
	}

	class CheckModule {
		private String attributeType;
		private boolean attribute = false; // Is it mandatory (true) or optional (false)
		private String format = null; // regular expression
		private String errorType; // type: warning, error
		private String errorMessage;

		public CheckModule(Element eElement) {
			attributeType = eElement.getAttribute("name");
			NodeList simpleSectNodes = eElement.getElementsByTagName("property");
			for (int i = 0; i < simpleSectNodes.getLength(); i++) {
				Element e = (Element) simpleSectNodes.item(i);
				if (e.hasAttribute("name") && e.hasAttribute("value")) {
					String name = e.getAttribute("name");
					String value = e.getAttribute("value");
					switch (name) {
					case "mandatory":
						this.attribute = (value.equals("1") ? true : false);
						break;
					case "format":
						this.format = value;
						break;
					case "type":
						this.errorType = value;
						break;
					case "message":
						this.errorMessage = value;
						break;
					}
				}
			}
		}
		public void check(javax.lang.model.element.Element e, javax.annotation.processing.Messager m) {
			if (e.getAnnotation(Config.class) != null) {
				String valueToCheck = "";
				switch (this.attributeType) {
				case "name":
					valueToCheck = ((Config) e.getAnnotation(Config.class)).name();
					break;
				case "defaultValue":
					valueToCheck = ((Config) e.getAnnotation(Config.class)).defaultValue();
					break;
				case "comment":
					valueToCheck = ((Config) e.getAnnotation(Config.class)).comment();
					break;
				case "forbidenValue":
					valueToCheck = ((Config) e.getAnnotation(Config.class)).constraint();
					break;
				}
				if (attribute 
						&& valueToCheck.isEmpty()) {
					m.printMessage(Kind.ERROR, "Undefined attribute \"" + this.attributeType + "\"", e);
				} else {
					if (! valueToCheck.isEmpty() ) {
						if (format != null) {
							Pattern pattern = Pattern.compile(format);
							Matcher matcher = pattern.matcher(valueToCheck);
							if (! matcher.matches()) {
								throwError(e, m);
							}
						}
					}
				}
			}
		}
		private void throwError (javax.lang.model.element.Element e, javax.annotation.processing.Messager m) {
			switch (errorType) {
			case "error":
				m.printMessage(Kind.ERROR, errorMessage, e);
				break;
			case "warning":
				m.printMessage(Kind.WARNING, errorMessage, e);
				break;
			default :
				m.printMessage(Kind.OTHER, errorMessage, e);
			}
		}

	}



}
