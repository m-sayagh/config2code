/**
 * @file CheckOptionType.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */
package ConfigCheckStyle;

import javax.annotation.processing.Messager;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic.Kind;

import Configuration.Annotations.Config;
//import Configuration.Annotations.ConfigType;

public class CheckOptionType implements IChecker {

	@Override
	public boolean check(Element e, Messager m) {
	/*
		if (e.getAnnotation(ConfigType.class) != null
				&& ! e.getSimpleName()
				.toString().equals(
						((ConfigType) e.getAnnotation(ConfigType.class)).name().toString())) {

			m.printMessage(Kind.ERROR, "The configuration option type "
					+ "should have the same name as the class name !"
					+ "\n"
					+ "Change the option type to " 
					+ e.getSimpleName()
					+ " Or change the class name to "
					+ ((ConfigType) e.getAnnotation(ConfigType.class)).name(), e);
			
			return false;
		}*/
		return true;
	}


}
