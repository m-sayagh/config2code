/**
 * @file NoNameSpaceTest.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Configuration.Injector.Flats;

import static org.junit.Assert.*;

import org.junit.Test;

import Configuration.Annotations.Config;

public class NoNameSpaceTest {

	private String configFile = "testResources/Flat/NoNameSpaces.txt";

	@Test
	public void test() {

		FlatOptions obj = new FlatOptions();
		Injector.ConfigInjector.injectConfig(obj, configFile, "flat" );

		if (obj.config1 == null || ! obj.config1.equals("test1")
				|| obj.config2 == null || ! obj.config2.equals("test2")
				|| obj.config3 == null || ! obj.config3.equals("test3")
				|| obj.config4 == null || ! obj.config4.equals("test4")
				)
			fail("Normal options injection is incorrect");
		assert(true);
	}
	
	
	class FlatOptions {

		@Config (name = "config1")
		public String config1;
		
		@Config (name = "config2")
		public String config2;
		
		@Config (name = "config3")
		public String config3;
		
		@Config (name = "config4")
		public String config4;
		
	}

}
