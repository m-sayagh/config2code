/**
 * @file CombinedTest.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Configuration.Injector.Flats;

import static org.junit.Assert.*;

import org.junit.Test;

import Configuration.Annotations.Config;
import Configuration.Annotations.Config.Support;

public class CombinedTest {

	private String configFile = "testResources/Flat/combined.txt";
	@Test
	public void test() {

		FlatOptions obj = new FlatOptions();
		Injector.ConfigInjector.injectConfig(obj, configFile, "flat" );
		
		

		if (obj.config1 == null || ! obj.config1.equals("test1")
				|| obj.config2 == null || ! obj.config2.equals("test2")
				|| obj.config3 == null || ! obj.config3.equals("test3")
				|| obj.config4 == null || ! obj.config4.equals("test4")
				|| obj.java == null || ! obj.java.equals("java")
				|| obj.javaVendor == null || ! obj.javaVendor.equals("vendor")
				|| obj.javaVendorUrl == null || ! obj.javaVendorUrl.equals("url")
				|| obj.username == null || ! obj.username.equals("username")
				|| obj.password == null || ! obj.password.equals("pwd")
				) {
			fail("Combined option types (namespace and ordinary options) injection is incorrect");
		}
		System.out.println(obj);
		assert(true);
	}


	class FlatOptions {

		@Config (name = "config1")
		public String config1;

		@Config (name = "config2")
		public String config2;

		@Config (name = "config3")
		public String config3;

		@Config (name = "config4")
		public String config4;


		@Config(name = "vendor", 
				type = String.class, 
				nameSpace="java")
		public String javaVendor;

		@Config(name = "java", 
				type = String.class)
		public String java;

		@Config(name = "url", 
				type = String.class, 
				nameSpace="java.vendor",
				support= Support.FILE)
		public String javaVendorUrl;

		@Config(name="name",
				nameSpace = "db.connexion.user")
		public String username;

		@Config(name="password",
				nameSpace = "db.connexion.user")
		public String password;
		@Config (name="test")
		public String test;
		
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "config1 = " + config1 + "\n" 
					+ "config2 = " + config2 + "\n"
					+ "config3 = " + config3 + "\n"
					+ "config4 = " + config4 + "\n"
					+ "java = " + java + "\n"
					+ "javaVendor = " + javaVendor + "\n"
					+ "javaVendorUrl = " + javaVendorUrl + "\n"
					+ "username = " + username + "\n"
					+ "password = " + password
					;
		}
	}

}
