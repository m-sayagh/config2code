/**
 * @file NameSpaceTests.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Configuration.Injector.Flats;

import static org.junit.Assert.*;

import org.junit.Test;

import Configuration.Annotations.Config;

public class NameSpaceTests {

	private String configFile = "testResources/Flat/nameSpace.txt";
	@Test
	public void test() {

		NameSpaceExample obj = new NameSpaceExample();
		Injector.ConfigInjector.injectConfig(obj, configFile, "flat" );

		if (obj.java == null || ! obj.java.equals("java")
				|| obj.javaVendor == null || ! obj.javaVendor.equals("vendor")
				|| obj.javaVendorUrl == null || ! obj.javaVendorUrl.equals("url")
				|| obj.username == null || ! obj.username.equals("username")
				|| obj.password == null || ! obj.password.equals("pwd")) {
			System.out.println(obj);
			fail("flat NameSpace options injection is incorrect");
		}
		System.out.println(obj);
		assert (true);
	}

	class NameSpaceExample {
		
		@Config(name = "vendor", 
				type = String.class, 
				nameSpace="java")
		public String javaVendor;
		
		@Config(name = "java", 
				type = String.class)
		public String java;
		
		@Config(name = "url", 
				type = String.class, 
				nameSpace="java.vendor")
		public String javaVendorUrl;
		
		@Config(name="name",
				nameSpace = "db.connexion.user")
		public String username;

		@Config(name="password",
				nameSpace = "db.connexion.user")
		public String password;
		
		@Override
		public String toString() {
			return "javaVendor = " + this.javaVendor + "\n"
					+ "java = " + this.java + "\n"
					+ "url = " + this.javaVendorUrl + "\n"
					+ "username = " + this.username + "\n"
					+ "password = " + this.password + "\n";
		}
		
		
	}
}
