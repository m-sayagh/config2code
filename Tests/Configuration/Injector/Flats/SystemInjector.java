/**
 * @file SystemInjector.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Configuration.Injector.Flats;

import static org.junit.Assert.*;

import org.junit.Test;

import Configuration.Annotations.Config;
import Configuration.Annotations.Config.Support;

public class SystemInjector {

	@Test
	public void test() {
		String configFile = "testResources/Flat/nameSpace.txt";

		TestClass obj = new TestClass();
		Injector.ConfigInjector.injectConfig(obj, configFile, "flat" );

		if (obj.javaVendorBugUrl == null || ! obj.javaVendorBugUrl.equals("http://bugreport.sun.com/bugreport/")) {
			fail ("system reading option failed !");
		}
		assert(true);
	}

	class TestClass {
		@Config(name = "name",
				type = String.class, 
				defaultValue = "999",
				comment = "totototo",
				nameSpace="db.connexion.user",
				support=Config.Support.FILE)
		String username;

		@Config (name = "bug", nameSpace= "java.vendor.url", support= Support.SYS)
		public String javaVendorBugUrl;
	}
}
