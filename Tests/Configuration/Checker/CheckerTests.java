/**
 * @file CheckerTests.java
 *
 * @copyright Copyright (C) 2019 config2code
 *
 * This file is part of the config2code framework
 *
 * config2code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * config2code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 */


package Configuration.Checker;

import static org.junit.Assert.*;

import org.junit.Test;

public class CheckerTests {
/*
	@Test
	public void NoNameSpace() {
		String configFile = "testResources/Flat/NoNameSpaces.txt";
		
		Model.Readers.ConfigReader reader = new Model.Readers.ConfigReader("flat",  configFile);
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config1", Configuration.Annotations.Config.Support.FILE), String.class, "test1")) {
			fail("Checker config1 is incorrect");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config2", Configuration.Annotations.Config.Support.FILE), String.class, "test2")) {
			fail("Checker config2 is incorrect");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config3", Configuration.Annotations.Config.Support.FILE), String.class, "test3")) {
			fail("Checker config3 is incorrect");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config4", Configuration.Annotations.Config.Support.FILE), String.class, "test4")) {
			fail("Checker config4 is incorrect");
		}
	}
	
	@Test
	public void NameSpace() {
		String configFile = "testResources/Flat/nameSpace.txt";
		
		Model.Readers.ConfigReader reader = new Model.Readers.ConfigReader("flat",  configFile);
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("java", Configuration.Annotations.Config.Support.FILE), String.class, "java")) {
			fail("Check 'java' failed ");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("java.vendor", Configuration.Annotations.Config.Support.FILE), String.class, "vendor")) {
			fail("Check 'java.vendor' failled ");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("java.vendor.url", Configuration.Annotations.Config.Support.FILE), String.class, "url")) {
			fail("Check 'java.vendor.url' failed");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("db.connexion.user.name", Configuration.Annotations.Config.Support.FILE), String.class, "username")) {
			fail("Check 'db.connexion.user.name' failed");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("db.connexion.user.password", Configuration.Annotations.Config.Support.FILE), String.class, "pwd")) {
			fail("Checker db.connexion.user.password failed");
		}
	}
	
	@Test
	public void Combined () {
		String configFile = "testResources/Flat/combined.txt";

		Model.Readers.ConfigReader reader = new Model.Readers.ConfigReader("flat",  configFile);
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config1", Configuration.Annotations.Config.Support.FILE), String.class, "test1")) {
			fail("Checker config1 is incorrect");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config2", Configuration.Annotations.Config.Support.FILE), String.class, "test2")) {
			fail("Checker config2 is incorrect");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config3", Configuration.Annotations.Config.Support.FILE), String.class, "test3")) {
			fail("Checker config3 is incorrect");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config4", Configuration.Annotations.Config.Support.FILE), String.class, "test4")) {
			fail("Checker config4 is incorrect");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("java", Configuration.Annotations.Config.Support.FILE), String.class, "java")) {
			fail("Check 'java' failed ");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("java.vendor", Configuration.Annotations.Config.Support.FILE), String.class, "vendor")) {
			fail("Check 'java.vendor' failled ");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("java.vendor.url", Configuration.Annotations.Config.Support.FILE), String.class, "url")) {
			fail("Check 'java.vendor.url' failed");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("db.connexion.user.name", Configuration.Annotations.Config.Support.FILE), String.class, "username")) {
			fail("Check 'db.connexion.user.name' failed");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("db.connexion.user.password", Configuration.Annotations.Config.Support.FILE), String.class, "pwd")) {
			fail("Checker db.connexion.user.password failed");
		}
	}
	
	@Test
	public void systemTest() {
		String configFile = "testResources/Flat/combined.txt";

		Model.Readers.ConfigReader reader = new Model.Readers.ConfigReader("flat",  configFile);
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config1", Configuration.Annotations.Config.Support.FILE), String.class, "test1")) {
			fail("Checker config1 is incorrect");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config2", Configuration.Annotations.Config.Support.FILE), String.class, "test2")) {
			fail("Checker config2 is incorrect");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config3", Configuration.Annotations.Config.Support.FILE), String.class, "test3")) {
			fail("Checker config3 is incorrect");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("config4", Configuration.Annotations.Config.Support.FILE), String.class, "test4")) {
			fail("Checker config4 is incorrect");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("java", Configuration.Annotations.Config.Support.FILE), String.class, "java")) {
			fail("Check 'java' failed ");
		}

	    if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("java.vendor.url.bug", Configuration.Annotations.Config.Support.SYS), String.class, "http://bugreport.sun.com/bugreport/")) {
	    	fail ("system option failed");
	    }
	    
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("java.vendor.url", Configuration.Annotations.Config.Support.FILE), String.class, "url")) {
			fail("Check 'java.vendor.url' failed");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("db.connexion.user.name", Configuration.Annotations.Config.Support.FILE), String.class, "username")) {
			fail("Check 'db.connexion.user.name' failed");
		}
		if (! CheckConfigValue.Checker.isForbidenValue(reader.getOption("db.connexion.user.password", Configuration.Annotations.Config.Support.FILE), String.class, "pwd")) {
			fail("Checker db.connexion.user.password failed");
		}
	}
*/
}
